(ns numeric-converters.core)

(defn bin-places [x]
  "Returns a list containing x binary places (e.g., 1 2 4 8 16 32)"
  (def places (list))
  (loop [y 1 cnt x]
    (if  (> cnt 0)
      (do
        (def places (cons y places))
        (recur (* y 2)(dec cnt)))))
  places
)

(defn bin-validate [x]
  "Test validity of binary number (passed in as a numeric list of its digits)"
  (def truths (map #(or (= 1 %) (= 0 %) ) x ))
  (if
    (some #(= false %)truths)
      (println "number is invalid")
      (println "number is valid")
  )
)

(defn bin2int [x]
  (def binvalues
    (map (fn [^Character c] (Character/digit c 10))(str x))
  )
  (bin-validate binvalues)
  (def binplaces
     (bin-places (count binvalues))
  )
  (reduce + (map * binvalues binplaces))
)

(def bintext (javax.swing.JOptionPane/showInputDialog "Enter binary number to convert:"))
(javax.swing.JOptionPane/showMessageDialog nil
  (str bintext " is equal to " (bin2int bintext) " in base 10"))     




